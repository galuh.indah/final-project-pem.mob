import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
          // menyusun tulisan secara horizontal dan di tengah
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Text('Dashboard'),
        ]),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications_active),
            onPressed: () {
              print('clik start');
            },
          ),
        ],
      ),
      drawer: new Drawer(
        //menggunakan listview karena di dalam drawer terdapat banyak menu
        child: new ListView(children: <Widget>[
          new UserAccountsDrawerHeader( //UserAccountsDrawerHeader sudah ada di fungsi widget
              accountName: new Text("Kadek Galuh Indah Kharismayanti"),
              accountEmail: new Text("galuh.indah@undiksha.ac.id"),
              currentAccountPicture: new GestureDetector(
                onTap: () {},//dapat melakukan apa 
                child: new CircleAvatar(
                  backgroundImage:new NetworkImage(
                    'https://images.app.goo.gl/NnfQ6qmyCMqqxbyG6'),
                ),
              ),
              decoration: BoxDecoration( //untuk background foto di drawer
                image: DecorationImage(
                    image: AssetImage('assets/appimages/latarr.jpeg'),
                    fit: BoxFit.cover),
              )),
          new ListTile( // listtile merupakan anak dari listview yang berupa menu
            title: new Text('Notifications'),
            trailing: new Icon(Icons.notifications_none),
          ),
          new ListTile(
            title: new Text('Wishlist'),
            trailing: new Icon(Icons.bookmark_border),
          ),
          new ListTile(
            title: new Text('Setting'),
            trailing: new Icon(Icons.settings),
          ),
          new ListTile(
            title: new Text('About '),
            trailing: new Icon(Icons.info),
          ),
          new ListTile(
            title: new Text(
                'Aplikasi toko baju ini merupakan aplikasi yang menjual aneka ragam atasan dan bawahan dengan kualitas terbaik dan harga terjangkau, biasanya hal ini hanya dijual di toko dan pasar tetapi dengan adanya aplikasi ini membuat pembeli bisa memesan melalui aplikasi'),
          ),
        ]),
      ),//enddrawer
//seluruh body dibungkus colum
      body: new ListView(
        children: <Widget>[
          Image.asset("assets/appimages/toko_baju.jpeg"),
//setiap bagian pada body dipisahkan container yang berisikan ringkasan aplikasi
          Container(
            color: Colors.yellow[300],
            padding: const EdgeInsets.all(10),
//untuk membuat tampilan secara horizontal digunakan row
            child: Row(
              children: [
                Expanded( // dihabiskan agar tidak terlalu kekiri dan terlalu kekanan
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Rp 200.000',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      Text(
                        'Points 15.000',
                        style: TextStyle(
                          color: Colors.black87,
                        ),
                      ),
                    ],
                  ),
                ),
                Icon(
                  Icons.local_offer,
                  color: Colors.red[500],
                ),
                Text('Beli  2 Gratis 1'),
              ],
            ),
          ),
//setiap bagian pada body dipisahkan container
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(color: Theme.of(context).dividerColor)),
            ),
//untuk membuat tampilan secara horizontal maka digunakan row
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
//untuk membuat tampilan secara vertikal maka digunakan colum
                Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.grid_on, color: Colors.blue),
                    Container(
                      margin: const EdgeInsets.only(top: 8),
                      child: Text(
                        "Semua List Barang",
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.fastfood_outlined, color: Colors.blue),
                    Container(
                      margin: const EdgeInsets.only(top: 8),
                      child: Text(
                        "Atasan Bawahan",
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.food_bank, color: Colors.blue),
                    Container(
                      margin: const EdgeInsets.only(top: 8),
                      child: Text(
                        "Atasan",
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.local_drink, color: Colors.blue),
                    Container(
                      margin: const EdgeInsets.only(top: 8),
                      child: Text(
                        "Bawahan",
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            decoration: new BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.yellow, Colors.orange],
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Jenis Model Baru',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      Text(
                        'Atasan dan Bawahan ',
                        style: TextStyle(
                          color: Colors.black87,
                        ),
                      ),
                    ],
                  ),
                ),
                Text('Silakan Di Order '),
              ],
            ),
          )
        ],
      ),
    );
  }
}
