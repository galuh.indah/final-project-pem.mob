import 'package:flutter/material.dart';
import './detail_produk.dart'; 

// menggunakan gesture detector dan navigator 
class ProdukList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Daftar List Barang")),
      body: ListView( 
        shrinkWrap: true,
        padding: const EdgeInsets.fromLTRB(2, 10, 2, 10),
        children: <Widget>[
          new GestureDetector( //menggunakan gesture detector agar dapat di klik
            onTap: () {
              Navigator.of(context).push( //menggunakan push agar dapat back ke halaman sebelumnya
                //navigator digunakan untuk menanggil sebuah fungsi atau class
                new MaterialPageRoute(
                    builder: (BuildContext context) => DetailProduk( //builder memanggil class yang ada di detail produk
                          //kemudian mengirim parameter, untuk menampilkan identitas
                          name: "ATASAN",
                          description: "Baju Kemeja",
                          price: 50000,
                          image: "baju_kemeja.jpeg",
                          star: 5,
                        )),
              );
            },
            //memanggil class local produkbox
            child: ProductBox(
              //berisikan parameter yang dibutuhkan produkbox
              nama: "ATASAN",
              deskripsi: "Baju Kemeja Perempuan dan Pria",
              harga: 50000,
              image: "baju_kemeja.jpeg",
              star: 5,
            ),
          ),
          new GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                new MaterialPageRoute(
                    builder: (BuildContext content) => DetailProduk(
                          name: "DRESS",
                          description: "Long Dres",
                          price: 70000,
                          image: "dres.jpeg",
                          star: 5,
                        )),
              );
            },
            child: ProductBox(
              nama: "DRESS",
              deskripsi: "Long Dres",
              harga: 70000,
              image: "dres.jpeg",
              star: 5,
            ),
          ),
          new GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                new MaterialPageRoute(
                    builder: (BuildContext content) => DetailProduk(
                          name: "T-SHIRT",
                          description: "Baju Kaus Pria ",
                          price: 35000,
                          image: "tshirt.jpeg",
                          star: 5,
                        )),
              );
            },
            child: ProductBox(
              nama: "T-SHIRT",
              deskripsi: "Baju Kaus Pria",
              harga: 35000,
              image: "tshirt.jpeg",
              star: 5,
            ),
          ),
          new GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                new MaterialPageRoute(
                    builder: (BuildContext content) => DetailProduk(
                          name: "CELANA PANJANG",
                          description: "Celana Panjang Kain Adem",
                          price: 80000,
                          image: "celana_panjang.jpeg",
                          star: 5,
                        )),
              );
            },
            child: ProductBox(
              nama: "CELANA PANJANG",
              deskripsi: "Celana Panjang Kain Adem",
              harga: 80000,
              image: "celana_panjang.jpeg",
              star: 5,
            ),
          ),
          new GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                new MaterialPageRoute(
                    builder: (BuildContext content) => DetailProduk(
                          name: "CELANA PENDEK",
                          description: "Bawahan Wanita dan Pria",
                          price: 50000,
                          image: "celana_pendek.jpeg",
                          star: 5,
                        )),
              );
            },
            child: ProductBox(
              nama: "CELANA PENDEK",
              deskripsi: "Bawahan Wanita dan Pria",
              harga: 50000,
              image: "celana_pendek.jpeg",
              star: 5,
            ),
          ),
          new GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                new MaterialPageRoute(
                    builder: (BuildContext content) => DetailProduk(
                          name: "ROK",
                          description: "Bawahan Rok Wanita",
                          price: 30000,
                          image: "rok.jpeg",
                          star: 5,
                        )),
              );
            },
            child: ProductBox(
              nama: "ROK",
              deskripsi: "Bawahan Rok Wanita",
              harga: 30000,
              image: "rok.jpeg",
              star: 5,
            ),
          )
        ],
      ),
    );
  }
}

class ProductBox extends StatelessWidget {
  ProductBox(
    //penangkapan parameter yang diterima dari myhomepage
      {Key key, this.nama, this.deskripsi, this.harga, this.image, this.star})
      : super(key: key);
  final String nama;
  final String deskripsi;
  final int harga;
  final String image;
  final int star;
  final children = <Widget>[];

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < star; i++) {
      children.add(new Icon(
        Icons.star,
        size: 10,
        color: Colors.yellow,
      ));
    }// for merupakan syntax yg digunakan untuk menampilkan berapa jumlah bintang

    return Container(
      padding: EdgeInsets.all(10),
      child: Row( 
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Image.asset( // menggunakan image lokal
            "assets/appimages/" + image,
            width: 150,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(5),
              child: Column(
                children: <Widget>[
                  Text(
                    this.nama, //untuk nama
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(this.deskripsi), //deskripsi produk
                  Text(
                    "Harga :" + this.harga.toString(), // harga produk
                    style: TextStyle(color: Colors.orange),
                  ),
                  Row(
                    children: <Widget>[
                      Row(
                        children: children,
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
